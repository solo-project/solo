ARG SOLO_VERSION=1.1.0.6
ARG SOLO_URL=https://files.minesolo.com/solo-v${SOLO_VERSION}-linux64.zip
ARG SOLO_SHA512=386a146f8f8223d8c0bf6c6921e1d1edf00e71bfb25995bc139194b1f397591a481270ad53a3dc1b4aabb822a71fed22697631c52c0b7e0f7b766a76ac9fa0aa

FROM ubuntu:18.04 as build

RUN apt-get update \
    && apt-get -qqy install --no-install-recommends --no-install-suggests \
        wget ca-certificates unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ARG SOLO_VERSION
ARG SOLO_URL
ARG SOLO_SHA512
RUN wget $SOLO_URL -O solo.zip \
    && echo $SOLO_SHA512 solo.zip | sha512sum -c - \
    && unzip solo.zip -d /solo \
    && rm solo.zip

FROM ubuntu:18.04 as solo

EXPOSE 22423

ARG SOLO_VERSION
COPY --from=build /solo /usr/local/bin

VOLUME /root/.solo

ENTRYPOINT [ "solod" ]
CMD [ "--p2p-bind-ip=0.0.0.0", \
      "--rpc-bind-ip=0.0.0.0", \
      "--confirm-external-bind"]
